(function () {

    ko.simpleGrid = {
        viewModel: function (configuration) {
            this.data = configuration.data;
            this.columns = configuration.columns;
        }
    };

    // Templates used to render the grid
    var templateEngine = new ko.nativeTemplateEngine();

    templateEngine.addTemplate = function(templateName, templateMarkup) {
        document.write("<script type='text/html' id='" + templateName + "'>" + templateMarkup + "<" + "/script>");
    };

    templateEngine.addTemplate("ko_simpleGrid_grid", "\
                    <table class=\"ko-grid\" cellspacing=\"0\">\
                        <thead>\
                            <tr data-bind=\"foreach: columns\">\
                               <th data-bind=\"text: headerText\"></th>\
                            </tr>\
                        </thead>\
                        <tbody data-bind=\"foreach: data\">\
                           <tr data-bind=\"foreach: $parent.columns\">\
                               <td >\
                                <!-- ko if: typeof rowText == 'function' --> \
                                    <img src=\"/resources/img/d.png\" data-bind=\"click: function() {rowText($parentContext.$index())} \" class=\"delete\"/>\
                                <!-- /ko -->\
                                <!-- ko ifnot: typeof rowText == 'function' --> \
                                    <span data-bind=\"text: $parent[rowText] \"></span>\
                                <!-- /ko -->\
                               </td>\
                            </tr>\
                        </tbody>\
                    </table>");




    // The "simpleGrid" binding
    ko.bindingHandlers.simpleGrid = {
        init: function() {
            return { 'controlsDescendantBindings': true };
        },
        // This method is called to initialize the node, and will also be called again if you change what the grid is bound to
        update: function (element, viewModelAccessor, allBindings) {
            var viewModel = viewModelAccessor();

            // Empty the element
            while(element.firstChild)
                ko.removeNode(element.firstChild);

            // Allow the default templates to be overridden
            var gridTemplateName = allBindings.get('simpleGridTemplate') || "ko_simpleGrid_grid";

            // Render the main grid
            var gridContainer = element.appendChild(document.createElement("DIV"));
            ko.renderTemplate(gridTemplateName, viewModel, { templateEngine: templateEngine }, gridContainer, "replaceNode");

        }
    };
})();
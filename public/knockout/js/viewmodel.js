var chart, chartData = [], templateEngine = new ko.nativeTemplateEngine();

viewModel = {
    data: ko.observableArray([]),
    columns: [
        { headerText: "Name", rowText: "name" },
        { headerText: "Quantity", rowText: "quantity" },
        { headerText: "Price", rowText: "price" },
        { headerText: "Delta", rowText: "delta" },
        { headerText: "Gamma", rowText: "gamma" },
        { headerText: "Theta", rowText: "theta" },
        { headerText: "VaR", rowText: "var" },
        { headerText: "", rowText: function(item) {
                viewModel.delete(item);
            }
        }
    ],
    itemName: ko.observable(""),
    itemQuantity: ko.observable(""),
    itemPrice: ko.observable("")
};

viewModel.chartData = ko.computed(function() {
    var acc = 0, input = viewModel.data();
    for (var i = 0; i < input.length; i++) {
        acc += parseFloat(input[i].price) *  parseFloat(input[i].quantity);
    }
    if (input.length) {
        chartData.push([chartData.length, acc]);
    }
    return chartData;
});

viewModel.delete = function (item) {
    $.ajax({
        url: '/data/'+ item,
        dataType: 'json',
        type: "DELETE"
    });
};

viewModel.add = function () {
    $.ajax({
        url: '/data',
        data: {
            name: viewModel.itemName(),
            quantity: viewModel.itemQuantity(),
            price: viewModel.itemPrice()
        },
        dataType: 'json',
        type: "POST",
        success: function () {
            viewModel.itemName("");
            viewModel.itemQuantity("");
            viewModel.itemPrice("");
        }
    });
};

ko.bindingHandlers.chartBinding = {
    init: function(element) {
        chart = $.plot(element, [], {series: {shadowSize: 0 }});
    },
    update: function(element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        chart.setData([value.chartData()]);
        chart.setupGrid();
        chart.draw();
    }
};

ko.bindingHandlers.simpleGrid = {
    init: function() {
        return { 'controlsDescendantBindings': true };
    },
    update: function (element, viewModelAccessor, allBindings) {
        var viewModel = viewModelAccessor();
        while(element.firstChild) {
            ko.removeNode(element.firstChild);
        }
        var gridTemplateName = allBindings.get('simpleGridTemplate') || "simpleGrid_grid";
        var gridContainer = element.appendChild(document.createElement("DIV"));
        ko.renderTemplate(gridTemplateName, viewModel, { templateEngine: templateEngine }, gridContainer, "replaceNode");
    }
};

ko.applyBindings(viewModel);

$(function () {
    setInterval( function() {
        $.ajax({
            url: '/data',
            dataType: 'json',
            success: function (result) {
                viewModel.data(result);
            }
        })
    }, 2000);
});

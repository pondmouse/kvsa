'use strict';

/* Controllers */

var app = angular.module('app', ['ngGrid']);

app.controller('appCtrl', function($scope, $http, $timeout) {

    var poll = function() {
        $timeout(function() {
            $http.get('/data').success(function(data) {
                $scope.feed = data;
                poll();
            });
        }, 2000);
    };
    poll();

    $scope.gridOptions = {
        data: 'feed',
        columnDefs: [
            {field: 'name', displayName: 'Name'},
            {field: 'quantity', displayName: 'Quantity'},
            {field: 'price', displayName: 'Price'},
            {field: 'delta', displayName: 'Delta'},
            {field: 'gamma', displayName: 'Gamma'},
            {field: 'theta', displayName: 'Theta'},
            {field: 'var', displayName: 'VaR'},
            {field:'', cellTemplate: '<img src="/resources/img/d.png" ng-click="delete(row)" class="delete"/>'}
        ]
    };

    $scope.delete = function(row) {
       $http.delete('/data/' + row.rowIndex);
    };

    $scope.add = function() {
        $http.post('/data', $scope.newTrade);
        $scope.newTrade = {};
    }

});

app.directive('myChart', function(){
    return{
        link: function(scope, elem, attrs){
            var chart = null, data = [], acc;
            scope.$watch('feed', function(result){
                if(!chart){
                    chart = $.plot(elem, data, {series: {shadowSize: 0 }});
                }else{
                    acc = 0;
                    for (var i = 0; i < result.length; i++) {
                        acc += parseFloat(result[i].price) *  parseFloat(result[i].quantity);
                    }
                    data.push([data.length, acc]);
                    chart.setData([data]);
                    chart.setupGrid();
                    chart.draw();
                }
            });
        }
    };
});
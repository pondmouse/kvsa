var express = require('express'),
    fs = require('fs'),
    app = express(),
    shares = require(__dirname + '/public/resources/data.json');

app.use(express.static(__dirname + '/public'));
app.use(express.bodyParser());

app.get('/data', function(req, res){
  for (var share in shares) {
      var sign = Math.random() < 0.5 ? -1 : 1, new_price, old_price = parseFloat(shares[share].price);
      new_price = parseFloat(old_price + sign * old_price * Math.random() * 0.001).toFixed(2);
      shares[share].price = new_price;
      shares[share].delta = (old_price / new_price).toFixed(5);
      shares[share].gamma = (old_price / new_price*.2).toFixed(5);
      shares[share].theta = (old_price / new_price*.025).toFixed(5);
      shares[share].var = (old_price / new_price*2).toFixed(5);
  }
  res.send(shares);
});

app.post('/data', function(req, res){
    shares.push(req.body);
    res.send(true);
});

app.delete('/data/:id', function(req, res) {
    if(shares.length <= req.params.id) {
        res.statusCode = 404;
        return res.send('Error 404: No quote found');
    }

    shares.splice(req.params.id, 1);
    res.json(true);
});

app.listen(process.env.PORT || 3000);
console.log('Listening on port 3000');